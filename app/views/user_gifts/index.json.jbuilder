json.array!(@user_gifts) do |user_gift|
  json.extract! user_gift, :id, :user_id, :gift_id
  json.url user_gift_url(user_gift, format: :json)
end
