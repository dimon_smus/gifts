json.array!(@gifts) do |gift|
  json.extract! gift, :id, :name, :visible, :not, :NULL, :default, :false
  json.url gift_url(gift, format: :json)
end
