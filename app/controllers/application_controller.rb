class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :check_role
  before_action :check_status
  # before_action :check_role

  # def check_role
  #   redirent_to root_url unless current_user.admin?
  # end

  # def current_user
  #   # find current user
  # end

  private

  def check_role
    unless !current_user.nil? && current_user.role.nil?
      return true
    end
  end

  def check_status
    if current_user && current_user.status == "banned"

      redirect_to root_path
    end
  end
end
