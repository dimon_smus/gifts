class Users::RegistrationsController < Devise::RegistrationsController
  # Override the action you want here.
  skip_before_action :check_status

  def create
    super

    UserMailer.welcome_email(params[:user]).deliver_now
  end
end