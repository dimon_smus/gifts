class UsersController < ApplicationController

  # NEED to avoid dead loop
  skip_before_action :authenticate_user!, only: :index
  skip_before_action :check_role, only: :index
  skip_before_action :check_status, only: :index
  # before_action , only:

  def index
    @users_list = User.all

    #user = {:email => "dmitriy.smus@gmail.com" }

    #mail = UserMailer.welcome_email(user)

    puts params

    @users_list = User.paginate(:page => params[:page], :per_page => 2)
    @q = @users_list.ransack(params[:q])
    #@users_list = @q.result()

    #binding.pry

  end

  def avatar

    user = User.find(params[:id])

    user.update(user_params)

    redirect_to root_path
  end

  def bann

    user = User.find(params[:id])

    if !user.role
      user.update_attribute(:status, :banned)
    end

    redirect_to root_path
  end

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  end

  # NEED WRITE
  def gifts

  end

  def delete

    @users_list = User.all.order(:id)

    redirect_to root_path
  end

  private

  def user_params
    params.require(:user).permit(:avatar)
  end

end