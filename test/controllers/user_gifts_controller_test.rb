require 'test_helper'

class UserGiftsControllerTest < ActionController::TestCase
  setup do
    @user_gift = user_gifts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:user_gifts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user_gift" do
    assert_difference('UserGift.count') do
      post :create, user_gift: { gift_id: @user_gift.gift_id, user_id: @user_gift.user_id }
    end

    assert_redirected_to user_gift_path(assigns(:user_gift))
  end

  test "should show user_gift" do
    get :show, id: @user_gift
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user_gift
    assert_response :success
  end

  test "should update user_gift" do
    patch :update, id: @user_gift, user_gift: { gift_id: @user_gift.gift_id, user_id: @user_gift.user_id }
    assert_redirected_to user_gift_path(assigns(:user_gift))
  end

  test "should destroy user_gift" do
    assert_difference('UserGift.count', -1) do
      delete :destroy, id: @user_gift
    end

    assert_redirected_to user_gifts_path
  end
end
