namespace :unbann do
  task :run => :environment do
    puts '*************************************'
    puts '======== START  TASK ========='

    users = User.all

    users.each do |user|
      unbann_user user
    end

    puts '======== FINISH TASK ========='
    puts '************************************'
  end

  def unbann_user(user)
    user.update_attribute(:status, "standard")
  end
end