class CreateUsers < ActiveRecord::Migration

  def up
    create_table :users do |t|
      t.boolean :role, null: false, default: false
      t.timestamps
    end
  end

  def change
    create_table :users do |t|
      t.boolean :role, null: false, default: false
      t.timestamps
    end
  end

  def down
    drop_table :users
  end
end
