class CreateGifts < ActiveRecord::Migration
  def change
    create_table :gifts do |t|
      t.string :name, null: false
      t.boolean :visible, null: false, default: false

      t.timestamps
    end
  end
end
