class UserStatus < ActiveRecord::Migration

  def self.up
    change_table :users do |t|
      t.string :status, default: 'standard'
    end
  end

  def self.down
    remove_attachment :users, :status
  end
end
