class CreateUserGifts < ActiveRecord::Migration
  def change
    create_table :user_gifts do |t|
      t.integer :user_id, null: false
      t.integer :gift_id, null: false

      t.timestamps null: false
    end
  end
end
